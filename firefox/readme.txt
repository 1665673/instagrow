
[安装]
(1)选择geckodriver.mac或者geckodriver.ubuntu，不带后缀名复制进$PATH目录，例如
cp geckodriver.mac /usr/local/bin/geckodriver
(2)安装火狐
apt-get install firefox

[命令行参数用法]
python3 like.py IG用户 IG密码 代理地址:端口:用户:密码 

[命令举例]
python3 like.py maxk1ckass 1234qwer zproxy.lum-superproxy.io:22225:lum-customer-hl_648f8412-zone-static-ip-38.131.154.233:9y4lv38oag4e


[可选代理]
zproxy.lum-superproxy.io:22225:lum-customer-hl_648f8412-zone-static-ip-185.217.61.22:9y4lv38oag4e

zproxy.lum-superproxy.io:22225:lum-customer-hl_648f8412-zone-static-ip-45.40.125.69:9y4lv38oag4e

zproxy.lum-superproxy.io:22225:lum-customer-hl_648f8412-zone-static-ip-181.214.186.227:9y4lv38oag4e

zproxy.lum-superproxy.io:22225:lum-customer-hl_648f8412-zone-static-ip-38.131.154.233:9y4lv38oag4e

zproxy.lum-superproxy.io:22225:lum-customer-hl_648f8412-zone-static-ip-181.214.183.143:9y4lv38oag4e